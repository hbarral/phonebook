# Test. Back-end PHP, MySQL, AJAX
A “Phone Book” website needs to be developed.
User, after registration and authorization, can manage the entries in his phone book.

##### Pages
- [x] User authorization (combined with the main page)
- [x] User registration
- [x] Phone book

##### User parameters
- [x] Login (Latin letters, numbers, up to 16 characters)
- [x] Email (correct email address)
- [x] Password (check for complexity - different registers + numbers, at least 6 characters)

##### Book entry options
- [x] Name
- [x] Surname
- [x] Phone number
- [x] Email (correct email address)
- [x] Image (jpeg or png, up to 5 mb)

##### Phone book functionality
- [x] List of subscribers
- [x] Adding an entry (do it without reloading using AJAX)
- [x] View recording
- [x] Delete entry

##### Terms
- [x] The task must be completed using PHP version 7.2 (or higher).
- [x] **It is impossible use PHP frameworks and libraries.**
- [x] For data storage - MySQL / MariaDB /  Percona server.
- [x] For the front end, you can use - Vanilla JS, jQuery, Vue.js. For layout
you can apply frameworks (Bootstrap, Bulma, Fondation, etc.).

##### I'd like to see:
- [x] Form validation,
- [x] PDO application,
- [x] Template implementation (MVC, Front controller, etc.).
- [x] Appearance is not evaluated, but consistency of interfaces is encouraged.

##### Result
Provide PHP archive and database dump. In the accompanying document
describe the specific startup conditions (if any).

Indicate how long you spent on completing the assignment.
v2.0 ©[Secret Company] 2020
