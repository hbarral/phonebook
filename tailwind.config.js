module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true
  },
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
    './src/**/*.js',
    './app/**/*.php'
  ],
  theme: {
    extend: {}
  },
  variants: {},
  plugins: []
}
