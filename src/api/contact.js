import request from '../utils/request'

export function contactSave(contact) {
  return request({
    url: '/contact/store',
    method: 'post',
    data: contact
  })
}

export function contactUpdate(id, contact) {
  return request({
    url: `/contact/update/${id}`,
    method: 'post',
    data: contact
  })
}

export function contactDelete(id) {
  return request({
    url: `/contact/delete/${id}`,
    method: 'post'
  })
}
