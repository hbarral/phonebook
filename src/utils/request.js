import axios from 'axios'

const service = axios.create({
  baseURL: location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : ''),
  timeout: 6000
})

service.interceptors.request.use(
  config => {
    config.headers.Accept = 'application/json'
    config.headers.ContentType = 'multipart/form-data'

    return config
  },
  error => {
    return Promise.reject(error)
  }
)

service.interceptors.response.use(
  response => {
    const res = response.data

    return res
  },
  error => {
    return Promise.reject(error)
  }
)

export default service
