import '@/styles/tailwind.css'
import { contactSave, contactUpdate, contactDelete } from './api/contact'

function showMessage(messages = [], type = 'success') {
  cleanMessages()
  const template = document.querySelector('#error-feedback')
  const feedback = document.querySelector('#error-feedback').cloneNode(true)
  feedback.removeAttribute('id')
  if (feedback.classList.contains('hidden')) {
    feedback.classList.remove('hidden')
  }
  if (!feedback.classList.contains('error-feedback')) {
    feedback.classList.add('error-feedback')
  }

  const ul = document.createElement('ul')
  ul.classList += 'pl-3'

  for (const msg of messages) {
    const li = document.createElement('li')
    li.innerHTML = msg
    ul.append(li)
  }

  feedback.className +=
    type === 'success'
      ? ' text-green-700 bg-green-100 rounded-md border border-green-200 border-solid'
      : ''
  feedback.className +=
    type === 'info'
      ? ' text-blue-700 bg-blue-100 rounded-md border border-blue-200 border-solid'
      : ''
  feedback.className +=
    type === 'warning'
      ? ' text-red-700 bg-red-100 rounded-md border border-red-200 border-solid'
      : ''

  feedback.append(ul.cloneNode(true))

  template.parentNode.insertBefore(feedback, template.nextSibling)
}

function cleanMessages() {
  const feedback = document.querySelectorAll('.error-feedback')

  for (const f of feedback) {
    f.remove()
  }
}

function showContact(contact) {
  const tbody = document.querySelector('#table-body')
  const tr = document.querySelector('#contact-template').cloneNode(true)
  tr.removeAttribute('id')
  if (tr.classList.contains('hidden')) {
    tr.classList.remove('hidden')
  }

  const buttons = tr.querySelectorAll('button')
  for (const b of buttons) {
    b.setAttribute('data-id', contact.id)

    if (b.classList.contains('btn-contact-view')) {
      b.addEventListener('click', viewContact)
    }

    if (b.classList.contains('btn-contact-edit')) {
      b.addEventListener('click', editContact)
    }

    if (b.classList.contains('btn-contact-delete')) {
      b.addEventListener('click', deleteContact)
    }
  }

  const name = tr.querySelector('[data-name]')
  const surname = tr.querySelector('[data-surname]')
  const phone = tr.querySelector('[data-phone]')
  const email = tr.querySelector('[data-email]')
  const avatar = tr.querySelector('[data-avatar]')

  name.innerHTML = contact.name
  surname.innerHTML = contact.surname
  phone.innerHTML = contact.phone
  email.innerHTML = contact.email
  avatar.src = contact.avatar

  tbody.append(tr)
}

function cleanForm() {
  document.querySelector('#form-add-contact input[name=name]').value = ''
  document.querySelector('#form-add-contact input[name=surname]').value = ''
  document.querySelector('#form-add-contact input[name=phone]').value = ''
  document.querySelector('#form-add-contact input[name=email]').value = ''
  document.querySelector('#picture-show').src = '/assets/images/default.jpg'
}

function selectPicture() {
  document.querySelector('#picture-file').click()
}

function showPicture() {
  const [file] = this.files
  if (!file) return

  const pictureShow = document.querySelector('#picture-show')
  const reader = new FileReader()

  reader.onload = () => {
    pictureShow.src = reader.result
  }
  reader.readAsDataURL(file)
}

function saveContact(e) {
  e.preventDefault()

  cleanMessages()

  const form = document.querySelector('#form-add-contact')
  const formData = new FormData(form)

  contactSave(formData)
    .then(data => {
      const ok = data?.ok
      const errors = data?.errors || []

      if (ok) {
        const message = data?.data?.message
        const contact = data?.data?.contact
        showMessage([message], 'success')
        setTimeout(() => cleanMessages(), 5000)
        showContact(contact)
        cleanForm()
      } else {
        showMessage(errors, 'warning')
      }
    })
    .catch(error => {
      console.log(error)
      showMessage(['Something went wrong'], 'warning')
    })
}

function updateContact(e) {
  e.preventDefault()

  cleanMessages()

  const form = document.querySelector('#form-update-contact')
  const formData = new FormData(form)
  const id = formData.has('id') ? formData.get('id') : ''

  if (!id) return

  contactUpdate(id, formData)
    .then(data => {
      const ok = data?.ok
      const errors = data?.errors || []

      if (ok) {
        const message = data?.data?.message
        showMessage([message], 'success')
        setTimeout(() => cleanMessages(), 5000)
      } else {
        showMessage(errors, 'warning')
      }
    })
    .catch(error => {
      console.log(error)
      showMessage(['Something went wrong'], 'warning')
    })
}

function deleteContact(e) {
  const target = e.target
  const id = target.dataset.id
  const row = target.parentNode.parentNode

  const response = prompt('Enter "yes" to confirm', 'no')

  if (response !== 'yes') return

  contactDelete(id)
    .then(data => {
      const ok = data?.ok
      const errors = data?.errors || []

      if (ok) {
        const message = data?.data?.message
        showMessage([message], 'info')
        setTimeout(() => cleanMessages(), 5000)
        row.remove()
      } else {
        showMessage(errors, 'warning')
      }
    })
    .catch(error => {
      console.log(error)
    })
}

function editContact(e) {
  const id = e.target.dataset.id

  window.location.href = `/contact/edit/${id}`
}

function viewContact(e) {
  const id = e.target.dataset.id

  window.location.href = `/contact/show/${id}`
}

const btn = document.querySelector('#add-contact')
if (btn) btn.addEventListener('click', saveContact)

const pictureSelector = document.querySelector('#picture-show')
if (pictureSelector) pictureSelector.addEventListener('click', selectPicture)

const inputPicture = document.querySelector('#picture-file')
if (inputPicture) inputPicture.addEventListener('change', showPicture)

const btnUpdate = document.querySelector('#update-contact')
if (btnUpdate) btnUpdate.addEventListener('click', updateContact)

const btnDelete = document.querySelectorAll('.btn-contact-delete')
for (const btn of btnDelete) {
  btn.addEventListener('click', deleteContact)
}

const btnEdit = document.querySelectorAll('.btn-contact-edit')
for (const btn of btnEdit) {
  btn.addEventListener('click', editContact)
}

const btnView = document.querySelectorAll('.btn-contact-view')
for (const btn of btnView) {
  btn.addEventListener('click', viewContact)
}
