<?php require APP_ROOT . '/Views/partials/header.php'; ?>

<main class="flex-1 overflow-y-auto px-5">

  <body class="antialiased font-sans bg-gray-200">
    <div class="container mx-auto px-4 sm:px-8">
      <div class="py-3">

        <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-2 flex flex-col justify-center items-center">

            <div class="max-w-sm md:hidden  bg-gray-200 align-left w-full py-2 pl-1" style="min-width: 380px;">
              <a href="/contact/index" class="bg-gray-500 text-gray-900 focus:outline-none">
                <svg viewBox="0 0 16 16" class="w-8 h-8" style="transform: rotate(180deg) scale(1, -1);">
                  <path d="M9.079 11.9l4.568-3.281a.719.719 0 0 0 0-1.238L9.079 4.1A.716.716 0 0 0 8 4.719V6c-1.5 0-6 0-7 8 2.5-4.5 7-4 7-4v1.281c0 .56.606.898 1.079.62z"/>
                </svg>
              </a>
            </div>

            <form id="form-update-contact">
              <div class="inline-block mt-2 min-w-full shadow rounded-lg overflow-hidden">
                <div class="border border-gray-300 p-6 grid grid-cols-1 gap-6 bg-white shadow-lg rounded-lg">
                <!-- message template -->
                <div class="whitespace-pre-wrap hidden md:block hidden" id="error-feedback"></div>
                <!-- message template -->
                  <div class="grid grid-cols-1 md:grid-cols-3 gap-4">

                    <div class="grid grid-cols-1 gap-2 border border-gray-200 p-2 rounded">
                      <div class="flex border rounded bg-gray-300 items-center p-2 ">
                        <svg class="fill-current text-gray-800 mr-2 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                          <path class="heroicon-ui"
                                d="M12 12a5 5 0 1 1 0-10 5 5 0 0 1 0 10zm0-2a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm9 11a1 1 0 0 1-2 0v-2a3 3 0 0 0-3-3H8a3 3 0 0 0-3 3v2a1 1 0 0 1-2 0v-2a5 5 0 0 1 5-5h8a5 5 0 0 1 5 5v2z"/>
                        </svg>
                        <input type="text" placeholder="Name (required)" name="name" value="<?php echo $data->name; ?>" class="bg-gray-300 max-w-full focus:outline-none text-gray-700"/>
                        <input type="text"  name="id" value="<?php echo $data->id; ?>" class="hidden"/>
                      </div>

                      <div class="flex border rounded bg-gray-300 items-center p-2 ">
                        <svg class="fill-current text-gray-800 mr-2 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                          <path class="heroicon-ui"
                                d="M12 12a5 5 0 1 1 0-10 5 5 0 0 1 0 10zm0-2a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm9 11a1 1 0 0 1-2 0v-2a3 3 0 0 0-3-3H8a3 3 0 0 0-3 3v2a1 1 0 0 1-2 0v-2a5 5 0 0 1 5-5h8a5 5 0 0 1 5 5v2z"/>
                        </svg>
                        <input type="text" placeholder="Surname" name="surname" value="<?php echo $data->surname; ?>" class="bg-gray-300 max-w-full focus:outline-none text-gray-700"/>
                      </div>

                    </div>

                    <div class="grid grid-cols-1 gap-2 border border-gray-200 p-2 rounded">

                      <div class="flex border rounded bg-gray-300 items-center p-2 ">
                        <svg class="fill-current text-gray-800 mr-2 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="24" height="24">
                          <path fill-rule="evenodd" d="M11 1H5a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM5 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H5z"/>
                          <path fill-rule="evenodd" d="M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                        </svg>
                        <input type="text" placeholder="Phone (required)" name="phone" value="<?php echo $data->phone; ?>" class="bg-gray-300 max-w-full focus:outline-none text-gray-700"/>
                      </div>

                      <div class="flex border rounded bg-gray-300 items-center p-2 ">

                        <svg class="fill-current text-gray-800 mr-2 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="24" height="24">
                          <path fill-rule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
                        </svg>

                        <input type="text" placeholder="Email" name="email" type="email" value="<?php echo $data->email; ?>" class="bg-gray-300 max-w-full focus:outline-none text-gray-700"/>
                      </div>


                    </div>

                    <div class="grid grid-cols-1 md:grid-cols-2 gap-2 border border-gray-200 p-2 rounded">
                      <div class="h-full w-full flex justify-center items-center">
                        <div class="group w-20 h-20 rounded-full overflow-hidden shadow-inner text-center bg-purple table cursor-pointer">
                          <img
                            src="<?php echo $data->avatar; ?>"
                            alt="contact picture"
                            class="object-cover object-center w-full h-full visible group-hover:hidden"
                            id="picture-show"
                            />
                        </div>
                        <input type="file" name="picture" id="picture-file" class="hidden" accept="image/png,image/jpeg,image/x-png">
                      </div>

                      <button id="update-contact" class="p-2 border w-full rounded-md bg-gray-800 text-white">
                        OK
                      </button>
                      <div class="whitespace-pre-wrap md:hidden block error-feedback"></div>
                    </div>

                  </div>
                </div>
              </div>
            </form>

            <div class="max-w-sm md:hidden  bg-gray-200 align-left w-full py-2 pl-1" style="min-width: 380px;">
              <a href="/contact/index" class="bg-gray-500 text-gray-900 focus:outline-none">
                <svg viewBox="0 0 16 16" class="w-8 h-8" style="transform: rotate(180deg) scale(1, -1);">
                  <path d="M9.079 11.9l4.568-3.281a.719.719 0 0 0 0-1.238L9.079 4.1A.716.716 0 0 0 8 4.719V6c-1.5 0-6 0-7 8 2.5-4.5 7-4 7-4v1.281c0 .56.606.898 1.079.62z"/>
                </svg>
              </a>
            </div>

        </div>
      </div>
    </div>
  </body>

</main>
<?php require APP_ROOT . '/Views/partials/footer.php'; ?>
