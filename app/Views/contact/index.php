<?php require APP_ROOT . '/Views/partials/header.php'; ?>

<main class="flex-1 overflow-y-auto p-5">

  <body class="antialiased font-sans bg-gray-200">
    <div class="container mx-auto px-4 sm:px-8">
      <div class="py-8">
        <div>
          <h2 class="text-2xl font-semibold leading-tight select-none">Contacts</h2>
        </div>

        <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-2 overflow-x-auto">
          <form id="form-add-contact">
            <div class="inline-block mt-2 min-w-full shadow rounded-lg overflow-hidden">
              <div class="border border-gray-300 p-6 grid grid-cols-1 gap-6 bg-white shadow-lg rounded-lg">
                <!-- message template -->
                <div class="whitespace-pre-wrap hidden md:block hidden" id="error-feedback"></div>
                <!-- message template -->
                <div class="grid grid-cols-1 md:grid-cols-3 gap-4">

                  <div class="grid grid-cols-1 gap-2 border border-gray-200 p-2 rounded">
                    <div class="flex border rounded bg-gray-300 items-center p-2 ">
                      <svg class="fill-current text-gray-800 mr-2 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                        <path class="heroicon-ui"
                              d="M12 12a5 5 0 1 1 0-10 5 5 0 0 1 0 10zm0-2a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm9 11a1 1 0 0 1-2 0v-2a3 3 0 0 0-3-3H8a3 3 0 0 0-3 3v2a1 1 0 0 1-2 0v-2a5 5 0 0 1 5-5h8a5 5 0 0 1 5 5v2z"/>
                      </svg>
                      <input type="text" placeholder="Name (required)" name="name" class="bg-gray-300 max-w-full focus:outline-none text-gray-700"/>
                    </div>

                    <div class="flex border rounded bg-gray-300 items-center p-2 ">
                      <svg class="fill-current text-gray-800 mr-2 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                        <path class="heroicon-ui"
                              d="M12 12a5 5 0 1 1 0-10 5 5 0 0 1 0 10zm0-2a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm9 11a1 1 0 0 1-2 0v-2a3 3 0 0 0-3-3H8a3 3 0 0 0-3 3v2a1 1 0 0 1-2 0v-2a5 5 0 0 1 5-5h8a5 5 0 0 1 5 5v2z"/>
                      </svg>
                      <input type="text" placeholder="Surname" name="surname" class="bg-gray-300 max-w-full focus:outline-none text-gray-700"/>
                    </div>

                  </div>

                  <div class="grid grid-cols-1 gap-2 border border-gray-200 p-2 rounded">

                    <div class="flex border rounded bg-gray-300 items-center p-2 ">
                      <svg class="fill-current text-gray-800 mr-2 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="24" height="24">
                        <path fill-rule="evenodd" d="M11 1H5a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM5 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H5z"/>
                        <path fill-rule="evenodd" d="M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                      </svg>
                      <input type="text" placeholder="Phone (required)" name="phone" class="bg-gray-300 max-w-full focus:outline-none text-gray-700"/>
                    </div>

                    <div class="flex border rounded bg-gray-300 items-center p-2 ">

                      <svg class="fill-current text-gray-800 mr-2 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="24" height="24">
                        <path fill-rule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
                      </svg>

                      <input type="text" placeholder="Email" name="email" type="email" class="bg-gray-300 max-w-full focus:outline-none text-gray-700"/>
                    </div>

                  </div>

                  <div class="grid grid-cols-1 md:grid-cols-2 gap-2 border border-gray-200 p-2 rounded">
                    <div class="h-full w-full flex justify-center items-center">
                      <div class="group w-20 h-20 rounded-full overflow-hidden shadow-inner text-center bg-purple table cursor-pointer">
                        <img
                           src="/assets/images/default.jpg"
                           alt="contact picture"
                           class="object-cover object-center w-full h-full visible group-hover:hidden"
                           id="picture-show"
                        />
                      </div>
                      <input type="file" name="picture" id="picture-file" class="hidden" accept="image/png,image/jpeg,image/x-png">
                    </div>

                    <button id="add-contact" class="p-2 border w-full rounded-md bg-gray-800 text-white">
                      Add
                    </button>
                    <div class="whitespace-pre-wrap md:hidden block error-feedback"></div>
                  </div>

                </div>
              </div>
            </div>
          </form>

          <div class="inline-block mt-2 min-w-full shadow rounded-lg overflow-hidden">
            <table class="min-w-full leading-normal">
              <thead>
                <tr>
                  <th
                    class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider select-none">
                    Name
                  </th>
                  <th
                    class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider select-none">
                    Surname
                  </th>
                  <th
                    class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider select-none">
                    Phone
                  </th>
                  <th
                    class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider select-none">
                    Email
                  </th>
                  <th
                    class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider select-none">
                    Actions
                  </th>
                </tr>
              </thead>
              <tbody id="table-body">
                <!-- template -->
                <tr id="contact-template" class="hidden">
                  <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <div class="flex items-center">
                      <div class="flex-shrink-0 w-10 h-10">
                        <img class="w-full h-full rounded-full"
                             src="/some/path/image.png"
                             alt="" data-avatar />
                      </div>
                      <div class="ml-3">
                        <p class="text-gray-900 whitespace-no-wrap">
                        <h1 data-name>
                          John
                        </h1>
                        </p>
                      </div>
                    </div>
                  </td>
                  <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">
                    <h1 data-surname>
                      Doe
                    </h1>
                    </p>
                  </td>
                  <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">
                    <h1 data-phone>
                      +7-111-222-33-44
                    </p>
                  </td>
                  <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">
                    <h1 data-email>
                      john@doe.org
                    </p>
                  </td>
                  <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <button class="btn-contact-view bg-teal-300 hover:bg-teal-500 text-teal-900 font-bold py-2 px-4 rounded inline-flex items-center outline-none focus:outline-none" data-id="1">
                      <svg viewBox="0 0 16 16" class="fill-current w-4 h-4 pointer-events-none"  xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                        <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                      </svg>
                    </button>

                    <button class="btn-contact-edit bg-orange-300 hover:bg-orange-500 text-orange-900 font-bold py-2 px-4 rounded inline-flex items-center outline-none focus:outline-none" data-id="1">
                      <svg viewBox="0 0 16 16" class="fill-current w-4 h-4 pointer-events-none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                      </svg>
                    </button>

                    <button class="btn-contact-delete bg-pink-300 hover:bg-pink-500 text-pink-900 font-bold py-2 px-4 rounded inline-flex items-center outline-none focus:outline-none" data-id="1">
                      <svg viewBox="0 0 16 16" class="fill-current w-4 h-4 pointer-events-none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                      </svg>
                    </button>
                  </td>
                </tr>
                <!-- template -->

                <?php foreach ($data as $contact) : ?>
                <tr>
                  <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <div class="flex items-center">
                      <div class="flex-shrink-0 w-10 h-10">
                        <img class="w-full h-full rounded-full"
                             src="<?php echo $contact->avatar; ?>"
                             alt="" data-avatar />
                      </div>
                      <div class="ml-3">
                        <p class="text-gray-900 whitespace-no-wrap">
                        <h1 data-name><?php echo $contact->name; ?></h1>
                        </p>
                      </div>
                    </div>
                  </td>
                  <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">
                    <h1 data-surname><?php echo $contact->surname; ?>
                    </h1>
                    </p>
                  </td>
                  <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">
                    <h1 data-phone><?php echo $contact->phone; ?>
                    </p>
                  </td>
                  <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <p class="text-gray-900 whitespace-no-wrap">
                    <h1 data-email><?php echo $contact->email; ?>
                    </p>
                  </td>
                  <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <button class="btn-contact-view bg-teal-300 hover:bg-teal-500 text-teal-900 font-bold py-2 px-4 rounded inline-flex items-center outline-none focus:outline-none" data-id="<?php echo $contact->id; ?>">
                      <svg viewBox="0 0 16 16" class="fill-current w-4 h-4 pointer-events-none"  xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                        <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                      </svg>
                    </button>

                    <button class="btn-contact-edit bg-orange-300 hover:bg-orange-500 text-orange-900 font-bold py-2 px-4 rounded inline-flex items-center outline-none focus:outline-none" data-id="<?php echo $contact->id; ?>">
                      <svg viewBox="0 0 16 16" class="fill-current w-4 h-4 pointer-events-none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                      </svg>
                    </button>

                    <button class="btn-contact-delete bg-pink-300 hover:bg-pink-500 text-pink-900 font-bold py-2 px-4 rounded inline-flex items-center outline-none focus:outline-none" data-id="<?php echo $contact->id; ?>">
                      <svg viewBox="0 0 16 16" class="fill-current w-4 h-4 pointer-events-none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                      </svg>
                    </button>
                  </td>
                </tr>
                <?php endforeach  ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>

</main>
<?php require APP_ROOT . '/Views/partials/footer.php'; ?>
