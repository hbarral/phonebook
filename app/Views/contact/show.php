<?php require APP_ROOT . '/Views/partials/header.php'; ?>

<main class="flex-1 overflow-y-auto px-5">

  <body class="antialiased font-sans bg-gray-200">
    <div class="container mx-auto px-4 sm:px-8">
      <div class="py-3">

        <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-2 flex flex-col justify-center items-center">

            <div class="max-w-sm md:hidden  bg-gray-200 align-left w-full py-2 pl-1" style="min-width: 380px;">
              <a href="/contact/index" class="bg-gray-500 text-gray-900 focus:outline-none">
                <svg viewBox="0 0 16 16" class="w-8 h-8" style="transform: rotate(180deg) scale(1, -1);">
                  <path d="M9.079 11.9l4.568-3.281a.719.719 0 0 0 0-1.238L9.079 4.1A.716.716 0 0 0 8 4.719V6c-1.5 0-6 0-7 8 2.5-4.5 7-4 7-4v1.281c0 .56.606.898 1.079.62z"/>
                </svg>
              </a>
            </div>

            <div class="max-w-sm bg-white shadow-lg rounded-lg overflow-hidden mb-4 mt-2" style="min-width: 380px;">
              <img class="w-full h-56 object-cover object-center select-none" src="<?php echo $data->avatar; ?>" alt="avatar">
              <div class="flex items-center px-6 py-3 bg-gray-900 relative">
                <a href="/contact/edit/<?php echo $data->id; ?>" class="-mt-6 p-4 bg-gray-500 mr-3 text-gray-900 rounded-full focus:outline-none absolute right-0">
                  <svg viewBox="0 0 16 16" class="w-6 h-6">
                    <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                  </svg>
                </a>
                <svg class="h-6 w-6 text-white fill-current" viewBox="0 0 16 16">
                  <path fill-rule="evenodd" d="M2.267.98a1.636 1.636 0 0 1 2.448.152l1.681 2.162c.309.396.418.913.296 1.4l-.513 2.053a.636.636 0 0 0 .167.604L8.65 9.654a.636.636 0 0 0 .604.167l2.052-.513a1.636 1.636 0 0 1 1.401.296l2.162 1.681c.777.604.849 1.753.153 2.448l-.97.97c-.693.693-1.73.998-2.697.658a17.47 17.47 0 0 1-6.571-4.144A17.47 17.47 0 0 1 .639 4.646c-.34-.967-.035-2.004.658-2.698l.97-.969z"/>
                </svg>
                <h1 class="mx-3 text-white font-semibold text-lg select-none"><?php echo $data->phone; ?></h1>
              </div>
              <div class="py-4 px-6">
                <h1 class="text-2xl font-semibold text-gray-800 select-none"><?php echo $data->name; ?></h1>
                <p class="pt-0 pb-2 text-lg text-gray-700 select-none"><?php echo $data->surname; ?></p>
                <div class="flex items-center mt-4 text-gray-700">
                  <svg class="h-6 w-6 fill-current" viewBox="0 0 16 16">
                    <path d="M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"/>
                  </svg>
                  <h1 class="px-2 text-sm select-none"><?php echo $data->id ?></h1>
                </div>
                <div class="flex items-center mt-4 text-gray-700">
                  <svg class="h-6 w-6 fill-current" viewBox="0 0 512 512">
                    <path d="M437.332 80H74.668C51.199 80 32 99.198 32 122.667v266.666C32 412.802 51.199 432 74.668 432h362.664C460.801 432 480 412.802 480 389.333V122.667C480 99.198 460.801 80 437.332 80zM432 170.667L256 288 80 170.667V128l176 117.333L432 128v42.667z"/>
                  </svg>
                  <a href="mailto:<?php echo $data->email; ?>" class="px-2 text-sm select-none"><?php echo $data->email; ?></a>
                </div>
              </div>
            </div>

            <div class="max-w-sm md:hidden  bg-gray-200 align-left w-full py-2 pl-1" style="min-width: 380px;">
              <a href="/contact/index" class="bg-gray-500 text-gray-900 focus:outline-none">
                <svg viewBox="0 0 16 16" class="w-8 h-8" style="transform: rotate(180deg) scale(1, -1);">
                  <path d="M9.079 11.9l4.568-3.281a.719.719 0 0 0 0-1.238L9.079 4.1A.716.716 0 0 0 8 4.719V6c-1.5 0-6 0-7 8 2.5-4.5 7-4 7-4v1.281c0 .56.606.898 1.079.62z"/>
                </svg>
              </a>
            </div>

        </div>
      </div>
    </div>
  </body>

</main>
<?php require APP_ROOT . '/Views/partials/footer.php'; ?>
