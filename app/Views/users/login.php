<?php require APP_ROOT . '/Views/partials/header.php'; ?>

<main class="flex-1 overflow-y-auto p-5 flex justify-center">
<div class="max-w-sm mx-auto px-6">
        <div class="relative flex flex-wrap">
            <div class="w-full relative">
                <div class="md:mt-6">
                    <div class="text-center font-semibold text-black select-none">
                      Login
                    </div>

                    <?php
                         use App\Helpers\Flash;

                           echo Flash::has('success') ? Flash::flash()->success() : '';
                           echo Flash::has('info') ? Flash::flash()->info() : '';
                           echo Flash::has('warning') ? Flash::flash()->warning() : '';
                           echo Flash::has('danger') ? Flash::flash()->danger() : '';

                         ?>

                    <form class="mt-8" action="/users/login" method="POST">
                        <div class="mx-auto max-w-lg ">
                            <div class="py-1">
                                <span class="px-1 text-sm text-gray-600 select-none">Email or username</span>
                                <input
                                      placeholder=""
                                      type="text"
                                      class="text-md block px-3 py-2 rounded-lg w-full bg-white border-2 border-gray-300 placeholder-gray-600 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-gray-600 focus:outline-none"
                                      name="login"
                                      required
                                      value="<?php echo $data['login']; ?>"
                                      >
                                      <span class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                                        <?php echo !empty($data['login_error']) ? $data['login_error'] : ''  ; ?>
                                      </span>

                            </div>
                            <div class="py-1">
                                <span class="px-1 text-sm text-gray-600 select-none">Password</span>
                                <input
                                      placeholder=""
                                      type="password"
                                      class="text-md block px-3 py-2 rounded-lg w-full bg-white border-2 border-gray-300 placeholder-gray-600 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-gray-600 focus:outline-none"
                                      name="password"
                                      required
                                      value="<?php echo $data['password']; ?>"
                                      >
                                      <span class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1">
                                        <?php echo !empty($data['password_error']) ? $data['password_error'] : ''  ; ?>
                                      </span>
                            </div>
                            <button
                              type="submit"
                              class="mt-3 text-lg font-semibold bg-gray-800 w-full text-white rounded-lg px-6 py-3 block shadow-xl hover:text-white hover:bg-black select-none">
                              Login
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
  </div>

</main>
<?php require APP_ROOT . '/Views/partials/footer.php'; ?>

