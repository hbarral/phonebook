<?php require APP_ROOT . '/Views/partials/header.php'; ?>

<main class="flex-1 overflow-y-auto p-5 flex justify-center">
  <div class="max-w-sm mx-auto px-6">
    <div class="relative flex flex-wrap">
      <div class="w-full relative">
        <div class="md:mt-2">
          <div class="text-center font-semibold text-black">
            Registration
          </div>
          <?php
               use App\Helpers\Flash;

               echo Flash::has('success') ? Flash::flash()->success() : '';
               echo Flash::has('info') ? Flash::flash()->info() : '';
               echo Flash::has('warning') ? Flash::flash()->warning() : '';
               echo Flash::has('danger') ? Flash::flash()->danger() : '';
          ?>
          <form
            class="mt-4"
            x-data="{password: '',password_confirm: ''}"
            action="/users/register"
            method="POST"
          >
            <div class="mx-auto max-w-lg ">
              <div class="py-1">
                <span class="px-1 text-sm text-gray-600">Name</span>
                <input
                  placeholder=""
                  type="text"
                  class="text-md block px-3 py-2 rounded-lg w-full bg-white border-2 border-solid placeholder-gray-600 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-gray-600 focus:outline-none border-gray-300"
                  name="name"
                  value="<?php echo $data['name']; ?>"
                />
                <span
                  class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1"
                >
                  <?php echo !empty($data['name_error']) ? $data['name_error'] : ''  ; ?>
                </span>
              </div>
              <div class="py-1">
                <span class="px-1 text-sm text-gray-600">Surname</span>
                <input
                  placeholder="<?php echo $data['surname']; ?>"
                  type="text"
                  class="text-md block px-3 py-2 rounded-lg w-full bg-white border-2 border-gray-300 placeholder-gray-600 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-gray-600 focus:outline-none"
                  name="surname"
                  value="<?php echo $data['surname']; ?>"
                />
                <span
                  class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1"
                >
                  <?php echo !empty($data['surname_error']) ? $data['surname_error'] : ''  ; ?>
                </span>
              </div>
              <div class="py-1">
                <span class="px-1 text-sm text-gray-600">Username</span>
                <input
                  placeholder=""
                  type="text"
                  class="text-md block px-3 py-2 rounded-lg w-full bg-white border-2 border-gray-300 placeholder-gray-600 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-gray-600 focus:outline-none"
                  name="username"
                  value="<?php echo $data['username']; ?>"
                />
                <span
                  class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1"
                >
                  <?php echo !empty($data['username_error']) ? $data['username_error'] : ''  ; ?>
                </span>
              </div>
              <div class="py-1">
                <span class="px-1 text-sm text-gray-600">Email</span>
                <input
                  placeholder=""
                  type="email"
                  class="text-md block px-3 py-2 rounded-lg w-full bg-white border-2 border-gray-300 placeholder-gray-600 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-gray-600 focus:outline-none"
                  name="email"
                  value="<?php echo $data['email']; ?>"
                />
                <span
                  class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1"
                >
                  <?php echo !empty($data['email_error']) ? $data['email_error'] : ''  ; ?>
                </span>
              </div>
              <div class="py-1">
                <span class="px-1 text-sm text-gray-600">Password</span>
                <input
                  placeholder=""
                  type="password"
                  x-model="password"
                  class="text-md block px-3 py-2 rounded-lg w-full bg-white border-2 border-gray-300 placeholder-gray-600 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-gray-600 focus:outline-none"
                  name="password"
                  value="<?php echo $data['password']; ?>"
                />
                <span
                  class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1"
                >
                  <?php echo !empty($data['password_error']) ? $data['password_error'] : ''  ; ?>
                </span>
              </div>
              <div class="py-1">
                <span class="px-1 text-sm text-gray-600">Password Confirm</span>
                <input
                  placeholder=""
                  type="password"
                  x-model="password_confirm"
                  class="text-md block px-3 py-2 rounded-lg w-full bg-white border-2 border-gray-300 placeholder-gray-600 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-gray-600 focus:outline-none"
                  name="password_confirm"
                  value="<?php echo $data['password_confirm']; ?>"
                />
                <span
                  class="flex items-center font-medium tracking-wide text-red-500 text-xs mt-1 ml-1"
                >
                  <?php echo !empty($data['password_confirm_error']) ? $data['password_confirm_error'] : ''  ; ?>
                </span>
              </div>
              <div class="flex justify-start mt-3 ml-4 p-1">
                <ul>
                  <li class="flex items-center py-1">
                    <div class="" class=" rounded-full p-1 fill-current "></div>
                    <span
                      :class="{'text-green-700': password == password_confirm && password.length > 0, 'text-red-700':password != password_confirm || password.length == 0}"
                      class="font-medium text-sm ml-3"
                      x-text="password == password_confirm && password.length > 0 ? 'Passwords match' : 'Passwords do not match' "
                    ></span>
                  </li>
                </ul>
              </div>
              <button
                type="submit"
                class="mt-3 text-lg font-semibold bg-gray-800 w-full text-white rounded-lg px-6 py-3 block shadow-xl hover:text-white hover:bg-black"
              >
                Register
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
</main>
<?php require APP_ROOT . '/Views/partials/footer.php'; ?>
