<?php

use App\Helpers\URL;

const SITE_NAME = 'PhoneBook';
define('APP_ROOT', dirname(__FILE__, 2));
define('SITE_URL', URL::getSiteURL());
define('PUBLIC_PATH', dirname(__FILE__, 3) . '/public');
define('PROJECT_PATH', dirname(__FILE__, 3));
const MAX_IMAGE_SIZE = 5242880;

const DB_HOST = 'localhost';
const DB_USER = 'root';
const DB_PASS = 'toor';
const DB_NAME = 'phonebook';
