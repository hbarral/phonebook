<?php

namespace App\Helpers;

class Validator {

  static $errors = true;

  static function check($arr, $on = false) {
    if ($on === false) {
      $on = $_REQUEST;
    }
    foreach ($arr as $value) {
      if (empty($on[$value])) self::throwError('Data is missing', 900);
    }
  }

  static function isError($array) : bool {
    foreach($array as $arr) {
      if (!empty($arr)) return true;
    }
    return false;
  }

  static function int($val) {
    $val = filter_var($val, FILTER_VALIDATE_INT);
    if ($val === false) {
      self::throwError('Invalid Integer', 901);
    }
    return $val;
  }

  static function string($val, $required = false, $error_message = null) {
    if (!$error_message) {
      $error_message = 'Invalid String';
    }
    if (!is_string($val)) {
      self::throwError('Invalid String' . $val . '.', 902);
    }
    if (empty($val) && $required) {
      self::throwError($error_message . $val . '.', 902);
    }

    return trim(stripslashes(htmlspecialchars($val)));
  }

  static function bool($val) {
    return filter_var($val, FILTER_VALIDATE_BOOLEAN);
  }

  static function email($val) {
    $val = filter_var($val, FILTER_VALIDATE_EMAIL);
    if ($val === false) self::throwError('Invalid Email', 903);

    return $val;
  }

  static function url($val) {
    $val = filter_var($val, FILTER_VALIDATE_URL);
    if ($val === false) self::throwError('Invalid URL', 904);

    return $val;
  }

  static function phone($phone) {
    if(!preg_match('/^\+[0-9]{1,2}-[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$/', $phone)) self::throwError('Invalid phone number format, use +7-111-222-33-44', 905);

    return $phone;
  }

  static function image($image) {
    if ($image['name']) {
      if ($image['size'] > MAX_IMAGE_SIZE) self::throwError('The image must be less than 5 MB.', 906);
      if (!in_array(mime_content_type($image['tmp_name']), ['image/jpeg', 'image/pjpeg', 'image/png'])) self::throwError('Invalid image format', 907);
    } else {
      self::throwError('This is not a valid image', 908);
    }
  }

  static function name(string $name, $required = true) {
    if ($required && empty($name)) self::throwError('Name is required', 909);
    if(!preg_match('/^[A-Za-z]+((\s)?([A-Za-z]*))*$/', $name)) self::throwError('Invalid name, use [a-z, A-Z]', 910);

    return $name;
  }

  static function surname(string $surname, $required = true) {
    if ($required && empty($surname)) self::throwError('Surname is required', 911);
    if(!preg_match('/^(\s)*[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])*))*(\s)*$/', $surname)) self::throwError('Invalid name, use [a-z, A-Z, \'-.]', 912);

    return $surname;
  }

  static function login(string $login, $required = true) {
    if ($required && empty($login)) self::throwError('Login is required', 913);

    $isMail = filter_var($login, FILTER_VALIDATE_EMAIL);
    $isUsername = preg_match('/^[A-Za-z0-9]{3,16}$/', $login);

    if (!$isMail && !$isUsername) self::throwError('The login requires a valid email or a username that contains letters and / or numbers. Minimum 3 and maximum 16 characters.', 914);

    return $login;
  }

  static function password($password) {
    if (empty($password)) self::throwError('Password is required', 917);

    $isValidPassword = preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$#", $password);

    if ($isValidPassword){
      return $password;
    } else {
      self::throwError('Weak password, use letters, numbers and at least one special character. Min. length 6, max. 20, example: P@ssW0rd-more-5ecUr3', 915);
    }
  }

  static function username(string $username) {
    $isUsername = preg_match('/^[A-Za-z0-9]{3,16}$/', $username);

    if (!$isUsername) self::throwError('Your username must contain letters and / or numbers. Minimum 3 and maximum 16 characters.', 916);

    return $username;
  }

  static function throwError($error = 'Error In Processing', $errorCode = 0) {
    if (self::$errors === true) throw new \Exception($error, $errorCode);
  }
}
