<?php

namespace App\Helpers;

class Auth
{
    function check()
    {
        return isset($_SESSION['user_id']) ? true : false;
    }
    function userId()
    {
        return $_SESSION['user_id'] ?? false;
    }

    function userName()
    {
      return $_SESSION['user_name'] ?? '';
    }
}
