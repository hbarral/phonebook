<?php

namespace App\Helpers;

class URL
{
    public static function getSiteURL()
    {
        $protocol = (!empty($_SERVER['HTTPS']) &&
            $_SERVER['HTTPS'] !== 'off' ||
            $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

        $domainName = $_SERVER['HTTP_HOST'];
        return $protocol.$domainName;
    }

    public static function redirect($page)
    {
        header('Location:' . $page);
        exit;
    }
}
