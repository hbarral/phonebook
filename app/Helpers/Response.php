<?php

namespace App\Helpers;

class Response
{
    static function json(array $data = [], int $code = 200)
    {
      header('Content-type: application/json');
      http_response_code($code);
      $response['ok'] = $data['ok'] ?? true;
      $response['errors'] = $data['errors'] ?? [];
      $response['data'] = $data['data'] ?? [];

      echo json_encode($response);
    }

    static function image(string $path, int $code = 200)
    {
      header('content-type: ' . mime_content_type($path));
      http_response_code($code);

      echo file_get_contents($path);
    }
}
