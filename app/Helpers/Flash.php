<?php

namespace App\Helpers;

class Flash
{
    private static $instance = null;

    public function __construct()
    {
        if (!headers_sent() && session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public static function flash()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        $numArgs= func_num_args();

        if ($numArgs == 1) {
            $_SESSION['type'] = 'success';
            $_SESSION['message'] = func_get_arg(0);
        }

        if ($numArgs == 2) {
            $_SESSION['type'] = func_get_arg(0);
            $_SESSION['message'] = func_get_arg(1);
        }

        return self::$instance;
    }

    public static function has(string $key) : bool
    {
        return (isset($_SESSION['type']) && $_SESSION['type'] == $key) ? true : false;
    }

    public static function success()
    {
        $type = $_SESSION['type'];
        $message = $_SESSION['message'];
        $template = <<<TEMPLATE
            <div class="alert-success flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-teal-900 bg-teal-200 border-t-4 border-teal-700 ">
                <div slot="avatar">
                    <svg
                        xmlns="http://www.w3.org/2000/svg" width="100%" height="100%"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        class="w-5 h-5 mx-2"
                        >
                        <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                        <polyline points="22 4 12 14.01 9 11.01"></polyline>
                    </svg>
                </div>
                <div class="text-xl font-normal  max-w-full flex-initial">
                    <div class="py-2">$type
                        <div class="text-sm font-base">
                            $message
                        </div>
                    </div>
                </div>
                <!-- <div class="flex flex-auto flex-row-reverse">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x cursor-pointer hover:text-green-400 rounded-full w-5 h-5 ml-2">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </div>
                </div> -->
            </div>
TEMPLATE;

        self::clearMessage();

        return $template;
    }

    public static function info()
    {
        $type = $_SESSION['type'];
        $message = $_SESSION['message'];
        $template = <<<TEMPLATE
            <div class="alert-info flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-blue-900 bg-blue-200 border-t-4 border-blue-700 ">
                <div slot="avatar">
                    <svg
                        viewBox="0 0 16 16"
                        class="w-5 h-5 mx-2"
                        fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg"
                        >
                        <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588z"/>
                        <circle cx="8" cy="4.5" r="1"/>
                    </svg>

                </div>
                <div class="text-xl font-normal  max-w-full flex-initial">
                    <div class="py-2">$type
                        <div class="text-sm font-base">
                            $message
                        </div>
                    </div>
                </div>
                <!-- <div class="flex flex-auto flex-row-reverse">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x cursor-pointer hover:text-green-400 rounded-full w-5 h-5 ml-2">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </div>
                </div> -->
            </div>
TEMPLATE;

        self::clearMessage();

        return $template;
    }

    public static function warning()
    {
        $type = $_SESSION['type'];
        $message = $_SESSION['message'];
        $template = <<<TEMPLATE
            <div class="alert-warning flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-yellow-900 bg-yellow-200 border-t-4 border-yellow-700 ">
                <div slot="avatar">
                    <svg
                        viewBox="0 0 16 16"
                        class="w-5 h-5 mx-2"
                        fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg"
                        >
                        <path fill-rule="evenodd" d="M4.54.146A.5.5 0 0 1 4.893 0h6.214a.5.5 0 0 1 .353.146l4.394 4.394a.5.5 0 0 1 .146.353v6.214a.5.5 0 0 1-.146.353l-4.394 4.394a.5.5 0 0 1-.353.146H4.893a.5.5 0 0 1-.353-.146L.146 11.46A.5.5 0 0 1 0 11.107V4.893a.5.5 0 0 1 .146-.353L4.54.146zM5.1 1L1 5.1v5.8L5.1 15h5.8l4.1-4.1V5.1L10.9 1H5.1z"/>
                        <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                    </svg>

                </div>
                <div class="text-xl font-normal  max-w-full flex-initial">
                    <div class="py-2">$type
                        <div class="text-sm font-base">
                            $message
                        </div>
                    </div>
                </div>
                <!-- <div class="flex flex-auto flex-row-reverse">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x cursor-pointer hover:text-green-400 rounded-full w-5 h-5 ml-2">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </div>
                </div> -->
            </div>
TEMPLATE;

        self::clearMessage();

        return $template;
    }

    public static function danger()
    {
        $type = $_SESSION['type'];
        $message = $_SESSION['message'];
        $template = <<<TEMPLATE
            <div class="alert-danger flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-red-900 bg-red-200 border-t-4 border-red-700 ">
                <div slot="avatar">
                    <svg
                        viewBox="0 0 16 16"
                        class="w-5 h-5 mx-2"
                        fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg"
                        >
                        <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                    </svg>

                </div>
                <div class="text-xl font-normal  max-w-full flex-initial">
                    <div class="py-2">$type
                        <div class="text-sm font-base">
                            $message
                        </div>
                    </div>
                </div>
                <!-- <div class="flex flex-auto flex-row-reverse">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x cursor-pointer hover:text-green-400 rounded-full w-5 h-5 ml-2">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </div>
                </div> -->
            </div>
TEMPLATE;

        self::clearMessage();

        return $template;
    }

    private function clearMessage()
    {
        unset($_SESSION['type']);
        unset($_SESSION['message']);
    }
}
