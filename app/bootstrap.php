<?php

include_once '../app/Core/autoload.php';
include_once '../app/Config/Config.php';

if (!headers_sent() && session_status() == PHP_SESSION_NONE) {
    session_start();
}
