<?php

namespace App\Core;

class BaseController
{
    public function index()
    {
    }

    public function model($model): object
    {
        $className = "\App\Models\\$model";

        return new $className;
    }

    public function view($view, $data = []): void
    {
        $viewFile = '../app/Views/' . $view . '.php';

        if (file_exists($viewFile)) {
            require_once $viewFile;
        } else {
            die('View does not exists');
        }
    }
}
