<?php
namespace App\Core;

class Core
{
    protected object $currentController;
    protected string $controllerName = 'Home';
    protected string $currentMethod = 'index';
    protected array $params = [];
    protected array $query = [];


    public function __construct()
    {
        $url = $this->getUrl();

        if (file_exists('../app/Controllers/' . ucwords($url['controller']) . '.php')) {
            $this->controllerName = ucwords($url['controller']);
            unset($url['controller']);
        }

        $className = "\App\Controllers\\$this->controllerName";

        $this->currentController = new $className;

        if (isset($url['method'])) {
            if (method_exists($this->currentController, $url['method'])) {
                $this->currentMethod = $url['method'];
                unset($url['method']);
            }
        }

        $this->params = isset($url['method_param']) ? [$url['method_param']] : [];
        $this->query = $url['query'] ?? [];

        call_user_func_array([$this->currentController, $this->currentMethod], [$this->params, $this->query]);
    }

    public function getUrl() : array
    {
        $url = $_SERVER['REQUEST_URI'] ?? null;
        $result = [];

        if (empty($url)) return [];

        $url = trim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);

        $path = parse_url($url, PHP_URL_PATH);
        $path = explode('/', $path);

        $result['controller'] = $path[0] ?? 'Home';
        $result['method'] = $path[1] ?? 'index';
        parse_str(parse_url($url, PHP_URL_QUERY), $result['query']);
        $result['method_param'] = $path[2] ?? null;

        return $result;
    }
}
