<?php

namespace App\Models;

use App\Core\Database;

class User
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function findUserByEmail($email)
    {
        $query = <<<SQL
                        SELECT *
                        FROM users
                        WHERE email = :email
                 SQL;

        $this->db->query($query);
        $this->db->bind(':email', $email);

        $this->db->single();

        return !!$this->db->rowCount();
    }

    public function findUserByLogin($login)
    {
        $query = <<<SQL
                        SELECT *
                        FROM users
                        WHERE (email = :login OR username = :login)
                 SQL;

        $this->db->query($query);
        $this->db->bind(':login', $login);

        $this->db->single();

        return !!$this->db->rowCount();
    }

    public function register($data)
    {
        $query = <<<SQL
                        INSERT INTO users (name, surname, username, email, password)
                        VALUES (:name, :surname, :username, :email, :password)
                 SQL;

        $this->db->query($query);
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':surname', $data['surname']);
        $this->db->bind(':username', $data['username']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);

        return $this->db->execute();
    }

    public function login($login, $password)
    {
        $query = <<<SQL
                        SELECT *
                        FROM users
                        WHERE (email = :login OR username = :login)
                 SQL;
        $this->db->query($query);
        $this->db->bind(':login', $login);

        $row = $this->db->single();

        $password_hashed = $row->password;

        return password_verify($password, $password_hashed)
            ? $row
            : false;
    }
}
