<?php

namespace App\Models;

use App\Core\Database;

class Contact
{
    private $db;

    function __construct()
    {
        $this->db = new Database;
    }

    function getUserContacts(int $user_id)
    {
        $this->db->query("SELECT * FROM contacts WHERE user_id = :user_id");
        $this->db->bind(':user_id', $user_id);

        return $this->db->resultSet();
    }

    function add($data)
    {
      $query = <<<SQL
                  INSERT INTO contacts (user_id, name, surname, phone, email, avatar)
                  VALUES (:user_id, :name, :surname, :phone, :email, :avatar)
               SQL;

      $this->db->query($query);
      $this->db->bind(':user_id', $data['user_id']);
      $this->db->bind(':name', $data['name']);
      $this->db->bind(':surname', $data['surname']);
      $this->db->bind(':phone', $data['phone']);
      $this->db->bind(':email', $data['email']);
      $this->db->bind(':avatar', $data['avatar']);

      $this->db->execute();

      return $this->findById($this->db->lastId());
    }

    function findById($id)
    {
      $query = <<<SQL
                  SELECT * FROM contacts WHERE id = :id
                  SQL;

      $this->db->query($query);
      $this->db->bind(':id', $id);

      return $this->db->single();
    }

    function findImageByName($imageName)
    {
      $query = <<<SQL
                  SELECT * from contacts WHERE avatar LIKE CONCAT('%', :image)
               SQL;

      $this->db->query($query);
      $this->db->bind(':image', $imageName);

      return $this->db->single();
    }

    function update(array $contact)
    {
      $query = <<<SQL
                  UPDATE contacts SET name = :name, surname = :surname, phone = :phone, email = :email, avatar = :avatar WHERE id = :id
               SQL;

      $this->db->query($query);

      $this->db->bind(':id', $contact['id']);
      $this->db->bind(':name', $contact['name']);
      $this->db->bind(':surname', $contact['surname']);
      $this->db->bind(':phone', $contact['phone']);
      $this->db->bind(':email', $contact['email']);
      $this->db->bind(':avatar', $contact['avatar']);

      return $this->db->execute();
    }

    function delete($id)
    {
      $query = <<<SQL
                  DELETE from contacts WHERE id = :id
               SQL;

      $this->db->query($query);

      $this->db->bind(':id', $id);

      return $this->db->execute();
    }
}
