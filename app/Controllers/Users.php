<?php

namespace App\Controllers;

use App\Core\BaseController;
use App\Helpers\{ Flash, URL, Validator, Request };

class Users extends BaseController
{
    public function __construct()
    {
        $this->userModel = $this->model('User');
    }

    public function register()
    {
        if (Request::isGet()) {
          $this->view('users/register');
        }

        if (Request::isPost()) {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [];

            try {
              $data['name'] = Validator::name($_POST['name']);
              $data['surname'] = Validator::surname($_POST['surname']);
              $data['username'] = Validator::username($_POST['username']);
              $data['email'] = Validator::email($_POST['email']);
              $data['password'] = Validator::password($_POST['password']);
              $data['password_confirm'] = Validator::password($_POST['password_confirm']);

            }
            catch(\Exception $e) {
              $data['name_error'] = ($e->getCode() == 909 || $e->getCode() == 910) ? $e->getMessage() : '';
              $data['name'] = ($e->getCode() == 909 || $e->getCode() == 910) ? $_POST['name'] : $data['name'];

              $data['surname_error'] = ($e->getCode() == 911 || $e->getCode() == 912) ? $e->getMessage() : '';
              $data['surname'] = ($e->getCode() == 911 || $e->getCode() == 912) ? $_POST['surname'] : $data['surname'];

              $data['username_error'] = ($e->getCode() == 916) ? $e->getMessage() : '';
              $data['username'] = ($e->getCode() == 916) ? $_POST['username'] : $data['username'];

              $data['email_error'] = ($e->getCode() == 903) ? $e->getMessage() : '';
              $data['email'] = ($e->getCode() == 903) ? $_POST['email'] : $data['email'];

              $data['password_error'] = ($e->getCode() == 915) ? $e->getMessage() : '';
              $data['password'] = ($e->getCode() == 915) ? $_POST['password'] : $data['password'];

              $data['password_confirm_error'] = ($e->getCode() == 915) ? $e->getMessage() : '';
              $data['password_confirm'] = ($e->getCode() == 915) ? $_POST['password_confirm'] : $data['password_confirm'];

              $this->view('users/register', $data);
            }

            try {
              $data['email_error'] = $this->userModel->findUserByEmail($data['email']) ? 'Email is already taken' : '';
              if (!empty($data['email_error'])) {
                $this->view('users/register', $data);
                exit;
              }
            }
            catch(\Exception $e) {
              die('Something went wrong');
            }

            $data['password_confirm_error'] = ($data['password'] != $data['password_confirm']) ? 'Passwords do not match' : '';
            if (!empty($data['password_confirm_error'])) {
              $this->view('users/register', $data);
              exit;
            }

            if (empty($data['name_error']) &&
                empty($data['surname_error']) &&
                empty($data['email_error']) &&
                empty($data['password_error']) &&
                empty($data['password_confirm_error'])
            ) {

              if(defined('PASSWORD_ARGON2ID')) {
                $data['password'] = password_hash($data['password'], PASSWORD_ARGON2ID);
              } else {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
              }

                if ($this->userModel->register($data)) {
                    Flash::flash('success', 'You have successfully registered');
                    URL::redirect('/users/login');
                } else {
                    die('Something went wrong');
                }
            } else {
                Flash::flash('warning', 'Please correct the errors indicated');
                $this->view('users/register', $data);
            }
        }
    }

    public function login()
    {
        if (Request::isPost()) {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [];

            try {
              $data['login'] = Validator::login($_POST['login']);
              $data['password'] = Validator::password($_POST['password']);
            }

            catch(\Exception $e) {
              $data['login_error'] = ($e->getCode() == 913 || $e->getCode() == 914) ? 'Wrong credentials' : '';
              $data['password_error'] = ($e->getCode() == 915) ? 'Wrong credentials' : '';
              $data['password_error'] = ($e->getCode() == 917) ? $e->getMessage() : 'Wrong credentials';

              $this->view('users/login', $data);
            }

            if (!$this->userModel->findUserByLogin($data['login'])) {
                $data['login_error'] = 'wrong credentials';
                $data['password_error'] = 'wrong credentials';

                $this->view('users/login', $data);
            }

            if (empty($data['login_error']) && empty($data['password_error'])) {
                $loggenInUser = $this->userModel->login($data['login'], $data['password']);

                if ($loggenInUser) {
                    $this->createUserSession($loggenInUser);
                    URL::redirect('/contact/index');
                } else {
                    $data['login_error'] = 'wrong credentials';
                    $data['password_error'] = 'wrong credentials';

                    $this->view('users/login', $data);
                }
            } else {
                $this->view('users/login', $data);
            }
        } else {
            $this->view('users/login');
        }
    }

    private function createUserSession($user)
    {
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_email'] = $user->email;
        $_SESSION['user_name'] = $user->name;
    }

    public function logout()
    {
        unset($_SESSION['user_id']);
        unset($_SESSION['user_email']);
        unset($_SESSION['user_name']);
        session_destroy();
        URL::redirect('/users/login');
    }
}
