<?php

namespace App\Controllers;

use App\Core\BaseController;
use App\Helpers\{ Auth, URL, Request, Validator, Response};

class Contact extends BaseController
{
    private $contactModel;

    function __construct()
    {
        if (!Auth::check()) return URL::redirect('/users/login');

        $this->contactModel = $this->model('Contact');
    }

    function index()
    {
      $user_id = Auth::userId();
      $data = $this->contactModel->getUserContacts($user_id);
      return $this->view('contact/index', $data);
    }

    function store()
    {

      if (Request::isPost()) {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        [
          'name' => $name,
          'surname' => $surname,
          'phone' => $phone,
          'email' => $email,
        ] = $_POST;

        try {
          $defaultPicture = '/contact/images/default.jpg';
          $inputPicture = $_FILES['picture'];
          $pictureName = basename($inputPicture['name']) ?? null;
          $target = '';
          $ext = '';
          $filename = '';
          $tempImage = '';
          $destinationFile = '';

          if ($pictureName) {
            $ext = pathinfo($pictureName, PATHINFO_EXTENSION);
            $imageSize = $inputPicture['size'] ?? null;
            $filename = uniqid('', true) . '.' . $ext;
            $target = '/contact/images/' . $filename;
            $tempImage = $inputPicture['tmp_name'];
            $destinationFile = PROJECT_PATH . '/storage' . $target;
            Validator::image($inputPicture);
          }

          $data = [
            'user_id' => Auth::userId(),
            'name' => Validator::string($name, true, 'The name field is required'),
            'surname' => empty($surname) ? '' : Validator::string($surname),
            'phone' => Validator::phone($phone),
            'email' => empty($email) ? '' : Validator::email($email),
            'avatar' => !empty($filename) ? $target : $defaultPicture
          ];

          if ($pictureName && $imageSize <= MAX_IMAGE_SIZE) {
            move_uploaded_file($tempImage, $destinationFile);
          }

        } catch(\Exception $e) {
          return Response::json(['ok' => false, 'errors' => [$e->getMessage()]]);
        }

        $errors[] = empty($name) ? 'Please enter a name' : '';
        if (Validator::isError($errors)) {
          return Response::json(['ok' => false, 'errors' => [$errors]]);
        }

        if ($contact= $this->contactModel->add($data)) {
          return Response::json(['data' => ['message' => 'Contact successfully saved', 'contact' => $contact]]);
        } else {
          return Response::json(['ok' => false, 'data' => ['message' => 'something went wrong']]);
        }
      }
    }

    function images($imageName)
    {
      $imageName = trim($imageName[0], '"');
      $row = $this->contactModel->findImageByName($imageName);

      if (!$row) {
        return Response::json(['ok' => true, 'errors' => ['message' => 'not found'], 'data' => []]);
      }

      if ($row->user_id === Auth::userId()) {
        $pathToImage = PROJECT_PATH . '/storage' . $row->avatar;

        return Response::image($pathToImage);
      }
    }

    function show($id)
    {
      $data = $this->contactModel->findById(...$id);

      return $this->view('contact/show', $data);
    }

    function edit($id)
    {
      $data = $this->contactModel->findById(...$id);

      return $this->view('contact/edit', $data);
    }

    function update(array $params)
    {
      if(Request::isPost())
      {
        $id = Validator::int($params[0]);

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        [
          'name' => $name,
          'surname' => $surname,
          'phone' => $phone,
          'email' => $email,
        ] = $_POST;

        $inputPicture = $_FILES['picture'];
        $pictureName = isset($inputPicture['name']) ? basename($inputPicture['name']) : null;
        $target = '';
        $ext = '';
        $filename = '';
        $tempImage = '';
        $destinationFile = '';

        try {
          $name = Validator::name($name);
          $surname = empty($surname) ? '' : Validator::surname($surname, false);
          $phone = Validator::phone($phone);
          $email = empty($email) ? '' : Validator::email($email);

          if ($pictureName) {
            $ext = pathinfo($pictureName, PATHINFO_EXTENSION);
            $filename = uniqid('', true) . '.' . $ext;
            $target = '/contact/images/' . $filename;
            $tempImage = $inputPicture['tmp_name'];
            $destinationFile = PROJECT_PATH . '/storage' . $target;
            Validator::image($inputPicture);
          }

          $contact = $this->contactModel->findById($id);

          if (Auth::userId() === $contact->user_id) {
            $data['id'] = $contact->id;
            $data['name'] = $name;
            $data['surname'] = $surname;
            $data['phone'] = $phone;
            $data['email'] = $email;
            $old_avatar = $contact->avatar;
            $data['avatar'] = !empty($filename) ? $target : $contact->avatar;


            $updated = $this->contactModel->update($data);

            if ($filename && $updated) {
            move_uploaded_file($tempImage, $destinationFile);
              if(is_writable($old_avatar)){
                unlink($old_avatar);
              }
            }

            $contact = $this->contactModel->findById($contact->id);

            return Response::json(['data' => ['message' => 'Contact successfully updated', 'contact' => $contact]]);
          }

        } catch (\Exception $e) {
          return Response::json(['ok' => false, 'errors' => [$e->getMessage()]]);
        }
      }
    }

    function delete($params)
    {
      if(Request::isPost())
      {
        $id = Validator::int($params[0]);

        try {
          $contact = $this->contactModel->findById($id);

          if (Auth::userId() === $contact->user_id) {
            $deleted = $this->contactModel->delete($id);

            if ($deleted) {
              if(is_writable($contact->avatar)){
                unlink($contact->avatar);
              }

              return Response::json(['data' => ['message' => 'Contact successfully deleted']]);
            }
          }
        }
        catch(\Exception $e) {
          return Response::json(['ok' => false, 'errors' => [$e->getMessage()]]);
        }
      }
    }
}
