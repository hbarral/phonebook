<?php

namespace App\Controllers;

use App\Core\BaseController;

class Home extends BaseController
{
    public function __construct()
    {
    }

    public function index()
    {
        $data = [
            'Добро пожаловать',
            'Welcome',
            'Bienvenido',
        ];

        return $this->view('home/index', $data);
    }
}
