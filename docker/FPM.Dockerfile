FROM php:7.4-fpm-alpine
RUN apk --update add curl
RUN set -ex \
  && apk --no-cache add \
  autoconf build-base
RUN pecl install xdebug
RUN docker-php-ext-install pdo pdo_mysql
RUN docker-php-ext-enable xdebug
ADD ./docker/conf/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
