# Phonebook App

Phonebook app for demonstration purposes according to specific requirements.
The requirements can be seen [here](./project_requirements.md)

## Live demo

https://labs.phonebook.hectorbarral.com

## Requirements
- docker (optional)
- git

## Installation

### with docker
```bash
git clone git@gitlab.com:hbarral/phonebook.git

cd phonebook

cp app/Config/Config.example.php app/Config/Config.php
cp .env.example .env
```
Adjust the variables in the **Config.php** and **.env** file according to your requirements (database username and password). Keep the database name, otherwise you will also have to modify **/docker/mysql/setup.sql**

And finally
```bash
docker-compose up --build
```
in another terminal and in the same directory

```bash
npm run dev
```

Now the application runs at http://localhost:8000

### without docker

Clone the project on your server, the entry point is at **/app/public/index.php**

Copy the configuration file
```bash
cp app/Config/Config.example.php app/Config/Config.php
```
Adjust the related variables to the database.

Initialize the database with the file **docker/mysql/setup.sql**

run

```bash
npm run prod
```

The application should be ready now.

## License
[MIT](https://choosealicense.com/licenses/mit/)
