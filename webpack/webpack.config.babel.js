import path from 'path'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
// import HtmlWebpackPlugin from 'html-webpack-plugin'
import { VueLoaderPlugin } from 'vue-loader'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import CopyPlugin from 'copy-webpack-plugin'
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin'
import TerserPlugin from 'terser-webpack-plugin'
import WriteFilePlugin from 'write-file-webpack-plugin'
import DotenvPlugin from 'webpack-dotenv-plugin'
import dotenv from 'dotenv'
dotenv.config({
  path: path.resolve(__dirname, '../.env')
})
import cssnano from 'cssnano'
import purgecss from '@fullhuman/postcss-purgecss'
import chokidar from 'chokidar'

const devMode = process.env.NODE_ENV !== 'production'

const config = {
  mode: process.env.NODE_ENV || 'development',
  entry: {
    'app': ['@babel/polyfill', path.resolve(__dirname, '../src/app.js')]
  },
  output: {
    filename: devMode ? 'js/[name].js' : 'js/[name].[hash].js',
    path: path.resolve(__dirname, '../public'),
    publicPath: '/'
  },
  resolve: {
    mainFiles: ['index'],
    alias: {
      '@': path.resolve(__dirname, '../src'),
      vue$: 'vue/dist/vue.runtime.esm.js'
    },
    extensions: [
      '.mjs',
      '.js',
      '.jsx',
      '.vue',
      '.json',
      '.wasm',
      'scss',
      'php'
    ]
  },
  devServer: {
    contentBase: path.resolve(__dirname, '../public'),
    index: 'index.html',
    port: process.env.PORT || 9000,
    stats: 'minimal',
    overlay: true,
    watchContentBase: true,
    before(app, server) {
      const files = [
        path.resolve(__dirname, '../app/**/*.php')
      ]
      chokidar
        .watch(files, {
          alwaysStat: true,
          atomic: false,
          followSymlinks: false,
          ignoreInitial: true,
          ignorePermissionErrors: true,
          persistent: true,
          usePolling: true
        })
        .on('all', () => {
          server.sockWrite(server.sockets, 'content-changed')
        })
    }
  },
  watchOptions: {
    aggregateTimeout: 200,
    poll: 1000,
    ignored: ['files/**/*.js', 'node_modules/**']
  },
  optimization: {
    minimize: true,
    minimizer: [
      new CssMinimizerPlugin({
        minimizerOptions: {
          preset: [
            'default',
            {
              discardComments: { removeAll: true }
            }
          ]
        }
      }),
      new TerserPlugin({
        terserOptions: {
          mangle: true,
          keep_classnames: false,
          keep_fnames: false,
          output: {
            comments: false
          }
        },
        extractComments: false
      })
    ]
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  require('tailwindcss')(path.resolve(__dirname, '../tailwind.config.js')),
                  !devMode
                    ? require('autoprefixer')
                    : null,
                  !devMode
                    ? cssnano({ preset: 'default' })
                    : null,
                  !devMode
                    ? purgecss({
                      content: ['./src/**/*.html', './src/**/*.vue', './src/**/*.jsx', './src/**/*.js', './app/Views/**/*.php', './src/**/*.scss'],
                      defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
                    })
                    : null
                ]
              }
            }
          }
        ]
      },
      {
        test: /\.module\.s(a|c)ss$/,
        loader: [
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
              sourceMap: devMode
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: devMode
            }
          }
        ]
      },
      {
        test: /\.s(a|c)ss$/,
        exclude: /\.module.(s(a|c)ss)$/,
        loader: [
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: devMode
            }
          }
        ]
      },
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin({
      dry: false,
      cleanOnceBeforeBuildPatterns: [],
      // cleanOnceBeforeBuildPatterns: [
      //   path.resolve(__dirname, '../public')
      // ],
      dangerouslyAllowCleanPatternsOutsideProject: true
    }),
    // new HtmlWebpackPlugin({
    //   showErrors: true,
    //   cache: true,
    //   filename: 'index.html',
    //   chunks: ['app'],
    //   title: process.env.APP_NAME || 'My App',
    //   description: process.env.DESCRIPTION || 'My Description',
    //   template: path.resolve(__dirname, '../src/index.html'),
    //   inject: true
    // }),
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: devMode ? 'css/[name].css' : 'css/[name].[hash].css',
      chunkFilename: devMode ? '[id].css' : '[id].[hash].css'
    }),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, '../src/assets/'),
          to: path.resolve(__dirname, '../public/assets/')
        },
        {
          from: path.resolve(__dirname, '../src/assets/images/favicon.ico'),
          to: path.resolve(__dirname, '../public/')
        },
        {
          from: path.resolve(__dirname, '../src/assets/images/default.jpg'),
          to: path.resolve(__dirname, '../storage/contact/images/')
        }
      ]
    }),
    new DotenvPlugin({
      path: path.resolve(__dirname, '../.env'),
      sample: path.resolve(__dirname, '../.env.example'),
      allowEmptyValues: true
    }),
    new WriteFilePlugin()
  ]
}

export default config
